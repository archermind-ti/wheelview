# WheelView

#### 介绍

一个在轮子上旋转的可选择的ohos小部件，类似于转动的ListView.

#### 功能演示

![](art/SOW-47_WheelView_p1.png)


![](art/SOW-47_WheelView_g1.gif)
![](art/SOW-47_WheelView_g2.gif)
![](art/SOW-47_WheelView_g3.gif)


#### 安装教程

1. 在module的build.gradle中添加对`WheelView`的依赖

    ```groovy
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    
       ……
       implementation 'com.gitee.archermind-ti:WheelView:1.0.0'
   }
   ```

2. 在project的build.gradle中添加`mavenCentral()`的引用

   ``` groovy
   allprojects {
       repositories {
           ……
           mavenCentral()
       }
   }
   ```

#### 使用说明

1) 在xml中使用
```xml
<DependentLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:app="http://schemas.huawei.com/res/ohos-auto"
    ohos:height="match_parent"
    ohos:width="match_parent">

    <com.lukedeighton.wheelview.WheelView
        ohos:id="$+id:wheelview"
        ohos:height="match_parent"
        ohos:width="match_parent"
        app:emptyItemColor="$color:green_900"
        app:wheelColor="$color:grey_400"
        app:rotatableWheelDrawable="false"
        app:selectionAngle="90.0"
        app:selectionColor="$color:teal_900"
        app:selectionPadding="4vp"
        app:repeatItems="true"
        app:wheelRadius="276vp"
        app:wheelItemCount="14"
        app:wheelPadding="13vp"
        app:wheelItemRadius="41vp"/>
</DependentLayout>
```

2) 类似于ListView一样设置一个 `WheelAdapter` 
```java
wheelView.setAdapter(new WheelAdapter() {
    @Override
    public String getDrawable(int position) {
        //return String here - the position can be seen in the gifs above
    }

    @Override
    public int getCount() {
        //return the count
    }
});
```

请注意，WheelAdapter的行为并不完全像ListAdapter，因为item不需要被回收。如果您需要刷新Adapter / Items，那么再次调用setAdapter。

3） Listeners
当与SelectionAngle最接近的项发生变化时的监听器。
```java
wheelView.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectListener() {
    @Override
    public void onWheelItemSelected(WheelView parent,  String itemDrawable, int position) {
        //the adapter position that is closest to the selection angle and it's String
    }
});
```

单击项时使用的侦听器
```java
wheelView.setOnWheelItemClickListener(new WheelView.OnWheelItemClickListener() {
    @Override
    public void onWheelItemClick(WheelView parent, int position, boolean isSelected) {
        //the position in the adapter and whether it is closest to the selection angle
    }
});
```
当轮子的角度改变时的监听器
```java
wheelView.setOnWheelAngleChangeListener(new WheelView.OnWheelAngleChangeListener() {
    @Override
    public void onWheelAngleChange(float angle) {
        //the new angle of the wheel
    }
});
```
#### 功能说明
##### Attributes

WheelView具有高度可定制性，可以通过xml或编程方式设置许多属性(建议使用xml，因为目前通过编程方式设置属性已经实现了一半)。以下是可以在xml中声明的自定义属性:

  * wheelDrawable
  * wheelColor
  * emptyItemDrawable
  * emptyItemColor
  * selectionDrawable
  * selectionColor
  * selectionPadding
  * selectionAngle
  * repeatItems
  * wheelRadius
  * wheelItemRadius
  * rotatableWheelDrawable
  * wheelOffsetX
  * wheelOffsetY
  * wheelItemCount
  * wheelItemAngle
  * wheelToItemDistance
  * wheelPosition
  * wheelPadding
  * wheelItemTransformer
  * selectionTransformer

5) WheelItemTransformer

确定与选择角度相关的“WheelItem”的绘制边界。

  * `SimpleItemTransformer` - 所有的items都是相同的尺寸
  * `ScalingItemTransformer` - items的大小在接近选择角度时增长

#### 版本迭代

- v1.0.0

License
-------
Copyright 2014 Luke Deighton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
