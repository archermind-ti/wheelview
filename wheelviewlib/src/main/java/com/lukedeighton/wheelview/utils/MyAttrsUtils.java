/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lukedeighton.wheelview.utils;

public interface MyAttrsUtils {
    String wheelDrawable ="wheelDrawable";
    String wheelColor ="wheelColor";
    String wheelItemTransformer ="wheelItemTransformer";
    String selectionTransformer ="selectionTransformer";
    String emptyItemDrawable ="emptyItemDrawable";
    String emptyItemColor ="emptyItemColor";
    String selectionDrawable ="selectionDrawable";
    String selectionColor ="selectionColor";
    String selectionPadding ="selectionPadding";
    String selectionAngle ="selectionAngle";
    String repeatItems ="repeatItems";
    String rotatableWheelDrawable ="rotatableWheelDrawable";
    String wheelRadius ="wheelRadius";




    String wheelItemRadius ="wheelItemRadius";
    String wheelOffsetX ="wheelOffsetX";
    String wheelOffsetY ="wheelOffsetY";
    String wheelItemCount ="wheelItemCount";
    String wheelItemAngle ="wheelItemAngle";
    String wheelItemAnglePadding ="wheelItemAnglePadding";
    String wheelToItemDistance ="wheelToItemDistance";


    String wheelPosition ="wheelPosition";





    String wheelPadding ="wheelPadding";
}
