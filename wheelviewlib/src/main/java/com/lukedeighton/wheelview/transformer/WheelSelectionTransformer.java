package com.lukedeighton.wheelview.transformer;


import com.lukedeighton.wheelview.WheelView;
import ohos.agp.components.element.Element;

public interface WheelSelectionTransformer {
    void transform(Element drawable, WheelView.ItemState itemState);
}
