package com.lukedeighton.wheelview.transformer;


import com.lukedeighton.wheelview.WheelView;
import ohos.agp.utils.Rect;

public interface WheelItemTransformer {
    /**
     * You have control over the Items draw bounds. By supplying your own WheelItemTransformer
     * you must call set bounds on the itemBounds.
     */
    void transform(WheelView.ItemState itemState, Rect itemBounds);
}
